import { UserDto } from "../dtos/user.dto";
import { User } from "../../domain/user";
import { Utils } from "../../../shared/application/utilities";
import { Mapper } from "../../../shared/application/mapper";

export class UserMapper extends Mapper {
    public map2UserDto(user:User){
        let userDto = new UserDto();
        userDto.setDocumentNumber(user.documentNumber);
        userDto.setUserId(user.id);
        userDto.setName(user.name);
        userDto.setSurname(user.surname)
        userDto.setType(user.type)
        return userDto
    }

    public map2Entity(req:any){
        let user = new User();
        user.type = req.userType
        user.documentNumber=req.documentNumber
        user.name = req.name
        user.surname = req.surname
        user.password = Utils.encrypt(req.password)
        return user
    }
}