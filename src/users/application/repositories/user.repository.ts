import { User } from "../../domain/user";
import { UserDto } from "../dtos/user.dto";

export interface UserRepository{
    getUserById(userId:number): Promise <User>
    getUserByCredentials(req: Object): Promise <User>
    getUserByDocumentNumber(documentNumber: String): Promise <User>
    createUser(user:User): Promise <UserDto>
}