import { BaseValidation } from "../../../validation/base.validation";

export class ValidateFieldsLogin extends BaseValidation{
    constructor(){
        super()
        let emptyLogin = this.fieldValidationFactory.createInstance('empty','$.documentNumber','El documento es requerido')
        let emptyPassword = this.fieldValidationFactory.createInstance('empty','$.password','El password es requerido')
        this.addValidator(emptyLogin)
        this.addValidator(emptyPassword)
    }

}