import { BaseValidation } from "../../../validation/base.validation";

export class ValidateFieldsRegisterUser extends BaseValidation{
    constructor(){
        super()
        let emptyLogin = this.fieldValidationFactory.createInstance('empty','$.documentNumber','El documento es requerido')
        let emptyPassword = this.fieldValidationFactory.createInstance('empty','$.password','El password es requerido')
        let emptyName = this.fieldValidationFactory.createInstance('empty','$.name','El nombre es requerido')
        let emptySurname = this.fieldValidationFactory.createInstance('empty','$.surname','El apellido es requerido')
        let emptyUserType = this.fieldValidationFactory.createInstance('empty','$.userType','El tipo de usuario (client, No client) es requerido')
        let userTypeValid = this.fieldValidationFactory.createInstance('userType','$.userType','El tipo de usuario es inválido');
        this.addValidator(emptyLogin)
        this.addValidator(emptyPassword)
        this.addValidator(emptyName)
        this.addValidator(emptySurname)
        this.addValidator(emptyUserType)
        this.addValidator(userTypeValid)
    }
}