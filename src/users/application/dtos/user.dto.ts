import { UserType, userType } from "../../domain/userType";

export class UserDto{
    private userId:number;
    private type: UserType;
    private documentNumber:string;
    private name: string;
    private surname: string;

    public getUserId(): number {
        return this.userId;
    }

    public setUserId(userId: number): void {
        this.userId = userId;
    }

    public getType(): UserType {
        return this.type;
    }

    public setType(type: UserType): void {
        this.type = type;
    }

    public getDocumentNumber(): string {
        return this.documentNumber;
    }

    public setDocumentNumber(documentNumber: string): void {
        this.documentNumber = documentNumber;
    }

    public getFullName(): string {
        return this.name+" "+this.surname;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getName(): string {
        return this.name;
    }

    public setSurname(surname: string): void {
        this.surname = surname;
    }

    public getSurname(): string {
        return this.surname;
    }



}