import { sequelize } from "../../../shared/infrastructure/base.persistence";
import { UserRepository } from "../../application/repositories/user.repository";
import { User } from "../../domain/user"
import { QueryTypes } from "sequelize"
import { UserModel } from "./models/user.model";
import { UserMapper } from "../../application/mappers/user.mapper";
import { UserDto } from "../../application/dtos/user.dto";

export class UserPersistence implements UserRepository{

    public async getUserByCredentials(req): Promise <User>{
        const users: any = await sequelize.query("SELECT * FROM `users` where type = ? AND document_number = ? AND password = ?", 
            { 
                replacements: ['Client',req.documentNumber, req.password],
                type: QueryTypes.SELECT 
            });
        
        return this.map2response(users)
    }

    public async getUserById(userId:number): Promise <User>{
        const users: any = await sequelize.query("SELECT * FROM `users` where type = ? AND id = ? ", 
            { 
                replacements: ['Client',userId],
                type: QueryTypes.SELECT 
            });
        return this.map2response(users)
    }

    public async getUserByDocumentNumber(documentNumber:string): Promise <User>{
        const users: any = await sequelize.query("SELECT * FROM `users` WHERE document_number = ?",{
            replacements:[documentNumber],
            type: QueryTypes.SELECT
        })
        return this.map2response(users)
    }

    public async createUser(user:User): Promise <UserDto>{
        let userModel = new UserModel()
        const newUser = await userModel.user.create({
            type:user.type,
            documentNumber: user.documentNumber,
            name: user.name,
            surname: user.surname,
            password: user.password,
        });
        let userRegistered:User = newUser.dataValues
        let mapper = new UserMapper();
        let userDto = mapper.map2UserDto(userRegistered)
        return userDto
    }

    private map2response(users:any): User{
        if(users.length===0)
            return new User
        let user:User = {
            id: users[0].id,
            type: users[0].type,
            documentNumber: users[0].document_number,
            name: users[0].name,
            surname: users[0].surname,
            password: users[0].password
        }
        return user
    }

    
}