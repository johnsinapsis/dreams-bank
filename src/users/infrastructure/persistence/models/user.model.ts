import { Sequelize, DataTypes, Model } from 'sequelize'
import { values } from 'sequelize/types/lib/operators'
import { sequelize } from '../../../../shared/infrastructure/base.persistence'

export class UserModel{
    
    public user

    constructor(){
        this.user = sequelize.define('user',{
            id: {
                type: DataTypes.INTEGER,
                autoIncrement:true,
                allowNull: false,
                primaryKey: true
            },
            type: {
                type: DataTypes.ENUM,
                values: ['Client','No Client']
            },
            documentNumber: {
                type: DataTypes.STRING,
                field: 'document_number',
            },
            name: {
                type: DataTypes.STRING,
            },
            surname: {
                type: DataTypes.STRING,
            },
            password: {
                type: DataTypes.STRING,
            }
        },
        {
            timestamps: false
        })
    }
}