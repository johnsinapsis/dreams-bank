import { Controller } from "../../controllers/controller.base";
import { ValidateFieldsRegisterUser } from "../application/validations/validateFIeldsRegisterUser";
import { BaseErrorHandler } from "../../shared/infrastructure/base.error.handler";
import { ErrorRegisterUser } from "./handler/error.register.user";
import { ExecRulesRegisterUser } from "./rules/exec.rules.registerUser";
import { UserMapper } from "../application/mappers/user.mapper";
import { User } from "../domain/user";
import { RegisterUser } from "../application/registerUser";
import { UserPersistence } from "./persistence/user.persistence";
import { UserDto } from "../application/dtos/user.dto";

export class RegisterUserPostController extends Controller{

    private validateFields
    private rules
    private mapper
    private daoUser

    constructor(){
        super();
        this.validateFields = new ValidateFieldsRegisterUser();
        this.rules = new ExecRulesRegisterUser();
        this.mapper = new UserMapper()
        this.daoUser = new UserPersistence()
    }

    getConfigId(){
        return 'registerUser'
    }

    async postRegister(req,res){
        try{
            if(this.validateFields.isValid(req,res)){
                await this.rules.exec(req.body)
                let user:User = this.mapper.map2Entity(req.body)
                let userDto: UserDto = await this.daoUser.createUser(user)
                let registerUser = new RegisterUser(userDto)
                let response = registerUser.map2Response() 
                return res.json(response);
            }
            else{
                let error = new ErrorRegisterUser();
                return res.json(error.buildResponse(res))
            }
        }
        catch(e){
            console.log(e);
            return BaseErrorHandler.handle(res,e)
        }
    }
}