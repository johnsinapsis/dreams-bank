import { ExecRulesNoUser } from "./exec.rules.noUser"

export class ExecRulesRegisterUser{

    private type = "ExecRulesRegisterUser"
    private rulesNoUser

    constructor(){
        this.rulesNoUser = new ExecRulesNoUser();
    }

    public async exec(req:any){
        await this.rulesNoUser.exec(req.documentNumber)
    }
}