import { BusinessNoUser } from "../../../shared/application/business.rules";
import { UserPersistence } from "../persistence/user.persistence"
import { BusinessError } from "../../../shared/application/handler/business.error";

export class ExecRulesNoUser implements BusinessNoUser{
    
    private type = "ExecRulesNoUser"
    private daoUser

    constructor(){
        this.daoUser = new UserPersistence()
    }

    public async noExistUser(documentNumber:String): Promise <boolean>{
        let user = await this.daoUser.getUserByDocumentNumber(documentNumber)
        if(user.id)
            return false
        else 
            return true;
    }

    public async exec(documentNumber:string){
        if(!await this.noExistUser(documentNumber))
            this.throwBusiness("El usuario ya se encuentra registrado")
    }

    public throwBusiness(msg){
        let response = {error: msg}
        throw new BusinessError(this.type,response)
    }

}