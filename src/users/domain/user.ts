import { UserType } from "./userType";

export class User{
    id: number
    type: UserType
    documentNumber: string
    name: string
    surname: string
    password: string
}