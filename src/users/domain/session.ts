export class Session{
    id: string
    userId: number
    dateStart: Date
    dateEnd: Date
}