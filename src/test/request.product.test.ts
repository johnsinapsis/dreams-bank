import { assert } from 'chai'
import faker from 'faker'
import { RequestProductUseCase } from './useCases/request.product'

let requestProduct = new RequestProductUseCase()
let requestNumber = 'AB'+faker.finance.account()
let requestNumberFail = "C65465465"
let userId = 2
let type = 3
let typeFail = 'ab'
let typeNotFound = 99
let result = null
//let params = {userId,type,accountNumber:requestNumber,state:'Pending'}
describe("Crear solicitud de nuevo producto",()=>{
    describe("Probando que el id de tipo de producto valide es numérico", ()=>{
        it("La validación debe devolver mensaje de error: El id del tipo de producto debe ser numérico", async()=>{
            requestProduct.init({userId,type:typeFail,requestNumber})
            result = await requestProduct.test()
            if(result)
                assert.equal(result['response']['error'],"El id del tipo de producto debe ser numérico")
        })
        
    })
    describe("Probando las validaciones de negocio",()=>{
        it("La validación debe devolver mensaje de error: El tipo de producto no existe", async()=>{
            requestProduct.init({userId,type:typeNotFound,requestNumber})
            result = await requestProduct.test()
            if(result)
                assert.equal(result['response']['error'],"El tipo de producto no existe")
        })
        it("La validación debe devolver mensaje de error: El producto ya existe", async()=>{
            requestProduct.init({userId,type,requestNumber:requestNumberFail})
            result = await requestProduct.test()
            if(result)
                assert.equal(result['response']['error'],"El producto ya existe")
        })
        it("Devuelve el mensaje: Se ha creado una nueva solicitud de producto", async()=>{
            requestProduct.init({userId,type,requestNumber})
            result = await requestProduct.test()
            if(result)
                assert.equal(result['response']['description'],"Se ha creado una nueva solicitud de producto")
        })
    })
})
