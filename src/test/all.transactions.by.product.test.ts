import { assert } from 'chai'
import { AllTransactionsByProductUseCase } from './useCases/all.transactions.by.product'

let allTransactionsByProduct = new AllTransactionsByProductUseCase()
let userId:number = 1
let productIdFail:string = 'abc'
let productId = 1
let productNotExist = 99
let result = null

describe("Obtener todas las transacciones relacionadas con el producto",()=>{
    describe("Probando que el id de producto valide es numérico", ()=>{
        it("La validación debe devolver mensaje de error: El id del producto debe ser numérico", async()=>{
            allTransactionsByProduct.init(userId,productIdFail)
            result = await allTransactionsByProduct.test()
            if(result)
                assert.equal(result['response']['error'],"El id del producto debe ser numérico")
        })
    })
    describe("Probando las validaciones de negocio",()=>{
        it("La validación debe devolver mensaje de error: El producto no existe", async()=>{
            allTransactionsByProduct.init(userId,productNotExist)
            result = await allTransactionsByProduct.test()
            if(result)
                assert.equal(result['response']['error'],"El producto no existe")
        })
        it("Devuelve el mensaje: Consulta realizada correctamente", async()=>{
            allTransactionsByProduct.init(userId,productId)
            result = await allTransactionsByProduct.test()
            if(result)
                assert.equal(result['response']['description'],"Consulta realizada correctamente")
        })
    })
})