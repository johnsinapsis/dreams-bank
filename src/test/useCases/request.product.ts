import axios from 'axios'
import { BaseUseCase } from './base.useCase'

export class RequestProductUseCase extends BaseUseCase{

    private route = "/products"
    private request = {}
    constructor(){
        super()
    }

    init(request){
        this.request = request
    }

    async test(){
        let route = this.url+this.route
        let res = await axios.post(route,this.request)
        return res.data
    }
}