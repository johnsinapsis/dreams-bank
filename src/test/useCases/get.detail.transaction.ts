import axios from 'axios'
import { BaseUseCase } from './base.useCase'

export class GetDetailTransactionUseCase extends BaseUseCase{

    private route = "/products/transaction"
    private transactionId
    constructor(){
        super()
    }

    init(transactionId){
        this.transactionId = transactionId
    }

    async test(){
        let route = this.url+this.route+'/'+this.transactionId
        let res = await axios.get(route)
        return res.data
    }
}