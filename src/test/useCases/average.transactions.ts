import axios from 'axios'
import { BaseUseCase } from './base.useCase'

export class AverageTransactionsUseCase extends BaseUseCase{

    private route = "/products/transactions/average"
    private request = {}
    constructor(){
        super()
    }

    init(request){
        this.request = request
    }

    async test(){
        let route = this.url+this.route
        let res = await axios.post(route,this.request)
        return res.data
    }
}