import { ConfigReader } from '../config/config'

export interface configControllerIdentity{
    getConfigId(): string
}

export class Controller {

    protected cr

    constructor(){
        this.cr = ConfigReader
    }

    getPathsMap(controllerConfigId){
        return this.cr.getPathsMap(controllerConfigId)
    } 

    build400Response(res){
        return { 
            "code" : 400,
            "invalid_fields": res.invalid_fields
        }
    }
}