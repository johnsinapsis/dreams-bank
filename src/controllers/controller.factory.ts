import { LoginPostController } from "../users/infrastructure/login.post.controller";
import { ShowAllProductsByUserGetController } from "../products/infrastructure/showAllProducts.get.controller";
import { ShowAllTransactionsByProductPostController } from "../products/infrastructure/showAllTransactions.post.controller";
import { ShowDetailTransactionGetController } from "../products/infrastructure/showDetailTransaction.get.controller";
import { AverageTransactionsPostController } from "../products/infrastructure/averageTransactions.controller";
import { RequestProductPostController } from "../products/infrastructure/requestProduct.post.controller";
import { RegisterUserPostController } from "../users/infrastructure/register.user.post.controller";

const CONTROLLERS = {
    LoginPostController,
    ShowAllProductsByUserGetController,
    ShowAllTransactionsByProductPostController,
    ShowDetailTransactionGetController,
    AverageTransactionsPostController,
    RequestProductPostController,
    RegisterUserPostController,
};

export class ControllerFactory {
    static createInstance(name) {        
        const cConstructor = CONTROLLERS[name];
        return cConstructor ? new cConstructor(name) : null;        
    }
}