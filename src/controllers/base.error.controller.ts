import { StackParser } from "../shared/infrastructure/base.error.handler";

export class BaseErrorController{
    protected type

    
    buildResponse(res){
        let response = {error:'',/* line:'', file:'' */}
        return this.toResponse(res, response)
    }

    public toResponse(res,response){
        let stack = new Error().stack
        let newRes = this.map2response(response,res.invalid_fields[0],stack)
        return {
            type: this.type,
            response:newRes
        } 
    }


    protected map2response(response,msg,stack){
        response.error = msg
        //por si se requiere detallar el error
        /* let properties = this.parseStack(stack)
        response.line = properties.line
        response.file = properties.file.substring(1) */
        return response
    }
    
    //se debe activar si se activa el detalle de errores
    /* private parseStack(stack) {
        let parser = new StackParser(stack)
        //let line = parser.getTopLine()
        let file = parser.getTopFile()
        return {line,file}
    } */


}