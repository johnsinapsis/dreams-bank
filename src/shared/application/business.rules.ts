export interface BusinessUser{
    existUser(userId:number): Promise <boolean>
}

export interface BusinessNoUser{
    noExistUser(documentNumber:String): Promise <boolean>
}

export interface BusinessSession{
    isSessionActiveUser(userId:number)
}

export interface BusinessProductType{
    existProductType(productType:number): Promise <boolean>
}

export interface BusinessProduct{
    existProduct(productId:number): Promise <boolean>
}

export interface BusinessNoProduct{
    noExistProduct(accountNumber:string): Promise <boolean>
}

export interface BusinessTransaction{
    existTransaction(transactionId:number): Promise <boolean>
}
