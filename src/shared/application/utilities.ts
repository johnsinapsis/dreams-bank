import { v4 as uuidv4 } from 'uuid'
import { UNDEF } from "../../config/constants";
import bcrypt from 'bcrypt'

export class Utils{
    static isEmpty(text) {
        return (Utils.isUndfNull(text) || text.toString() == "")
    }

    static isUndfNull(text) {
        return (typeof text == UNDEF || text == null)
    }

    static uuidv4(){
        return uuidv4()
    }

    static isNumber(text) {
        if (!Utils.isEmpty(text)) {
            return !isNaN(text)
        }
        return false
    }
    
    static isDate(date){
        if (!/\d{4}-\d{2}-\d{2}/.test(date)) return false;
        let d = new Date(date)
        if(isNaN(d.getTime())) return false
        return true
    }

    static isSuccessRange(dateStart,dateEnd){
        let f1 = new Date(dateStart)
        let f2 = new Date(dateEnd)
        return !(f2<f1)
    }


    static setLocalDate(date){
        let z = date.getTimezoneOffset() * 60 * 1000
        let tLocal = date - z
        let localD = new Date(tLocal)
        return localD
    }

    static encrypt(password){
        return bcrypt.hashSync(password,10)
    }

    static compareEncrypt(p1,p2){
        return bcrypt.compareSync(p1,p2)
    }


}