import jwt from 'jsonwebtoken'
import config  from '../../config/config'
import { UserDto } from '../../users/application/dtos/user.dto'
import { Session } from '../../users/domain/session'

export class Authentication{

    private apiKey = config.auth.token
    private time = config.auth.time+'d'

    public sign(user:UserDto,session:Session){
        return jwt.sign({user,session},this.apiKey,{expiresIn:this.time});
    }

    public validateToken(token,res){
        let decoded =jwt.verify(token,this.apiKey,(error,decoded)=>{
            if(error)
                return res.status(401).json({error:"Token de autenticación no válido"})
            return decoded
        })
        return decoded
    }

}