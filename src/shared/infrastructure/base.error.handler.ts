import { EnvConfig } from "../../config/config"
import { BusinessErrorHandler } from "./business.error.handler"
import { BusinessError } from "../application/handler/business.error"



export class StackParser {

    protected frames = []
    protected config = { "skip_js":true, 
                         "max_line":120,
                         "source_path":true }

    constructor(stack?) {
        this.load(stack)
    }

    load(stack){
        this.frames = []
        if (stack) {
            for (let frameTxt of stack.split("\n")) {
                let idx = frameTxt.lastIndexOf(EnvConfig.pathSeparator())
                if (idx > 0) {
                    let frame:any = {}
                    frame.textLine = frameTxt
                    let parts =frame.textLine.split('(')                    
                    if(parts.length ==2){
                        frame.classPath = parts[0]
                        frame.sourcePath = parts[1].replace(')', '')
                    }
                    frameTxt = frameTxt.substring(idx)
                    parts = frameTxt.split(':')
                    if(parts.length ==3){
                        frame.lineNumber = parts[1]
                        frame.lineColumn = parts[2].replace(')', '')
                        frame.file = parts[0].replace('/', '')
                    }
                    this.frames.push(frame)
                }
            }
        }
        
        return this
    }
    
    loggerProxy(){
        return { 'err': console.log }
    }
    
    printStack(loggerP?){
        let logger = loggerP? loggerP : this.loggerProxy()
        if(this.config.skip_js){
            let max = this.config.max_line
            for(let frame of this.frames){
                if(frame.file.endsWith('.ts') && !this.config.source_path ){
                    let truncated = frame.textLine.length >max ? frame.textLine.substring(frame.textLine.length-max): frame.textLine                  
                    logger.err(truncated)
                }else if (frame.file.endsWith('.ts') ){
                    logger.err(frame.sourcePath)
                }
            }
        }
    }

    printFrame(frame){
        console.log( frame.file + '( ' + frame.lineNumber + ' )' + ', ( ' + frame.lineColumn + ' )')        
    }

    public getTopLine(){
        return this.frames[0] ? this.frames[0].lineNumber : ''
    }

    public getTopFile(){        
        return this.frames[0] ? this.frames[0].file : ''
    }

}

export class BaseErrorHandler {

    static beh = new BusinessErrorHandler()

    static handle(res, e) {
        if (e instanceof BusinessError) {
            let properties = this.parseStack(e.response)
            this.clear(e)
            return res.status(400).json(e)
        }
        else{
            return res.status(400).json({error:"Excepción no controlada del sistema"})
        }
    }

    static parseStack(e) {
        let parser = new StackParser(e.stack)
        let line = parser.getTopLine()
        let file = parser.getTopFile()
        return {line,file}
    }

    static clear(e){
        delete e.code
        delete e.response.stack
    }

}
