import { EmptyFieldValidation } from "./field/empty.field.validation"
import { NumberFieldValidation } from "./field/number.field.validation"
import { DateFieldValidation } from "./field/date.field.validation"
import { RangeDateFieldsValidation } from "./field/rangeDate.fields.validation"
import { UserTypeValidation } from "./field/user.type.validation"

export const FIELD_VALIDATORS = {
    "empty": EmptyFieldValidation,    
    "number": NumberFieldValidation,    
    "date": DateFieldValidation,    
    "range-date": RangeDateFieldsValidation, 
    "userType": UserTypeValidation 
}

export class FieldValidationFactory{
    static createInstance(name, jpExpr, failMsg) {
        const fConstructor = FIELD_VALIDATORS[name]
        return fConstructor ? new fConstructor(jpExpr, failMsg) : null
    }

}