import { FieldValidator } from '../field/field.validation'
import { Utils } from '../../shared/application/utilities'
import jp from 'jsonpath'
import { INV_FIELDS, UNDEF } from '../../config/constants'
import { userType ,UserType } from '../../users/domain/userType'

const isUserType = (x:any): x is UserType => userType.includes(x)


/**
 * Valida que tipo de dato sea el tipo de cliente
 */

export class UserTypeValidation extends FieldValidator{

    constructor(jpExpr, failMsg){
        super(jpExpr, failMsg)
        this.jpExpr = jpExpr
        this.failMsg = failMsg
    }

    isValid(req, res){
        let field = jp.query(req.body, this.jpExpr)[0]
        if(!isUserType(field))
            return false;
        return true;
    }

    addErrorResponse( res, msg ){        
        if( typeof res[INV_FIELDS] === UNDEF ){
            res.invalid_fields = []
        }
        res.invalid_fields.push(msg)
    }
}