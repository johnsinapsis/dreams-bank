import { FieldValidator } from '../field/field.validation'
import { Utils } from '../../shared/application/utilities'
import jp from 'jsonpath'
import { INV_FIELDS, UNDEF } from '../../config/constants'

/**
 * Valida el rango de fechas teniendo en cuenta que los campos son dateStart y dateEnd
 */
export class RangeDateFieldsValidation extends FieldValidator {

    constructor(jpExpr, failMsg){
        super(jpExpr, failMsg)
        this.jpExpr = jpExpr
        this.failMsg = failMsg
    }

    isValid(req, res){
        let dateStart = jp.query(req.body, this.jpExpr)[0].dateStart
        let dateEnd = jp.query(req.body, this.jpExpr)[0].dateEnd
        return Utils.isSuccessRange( dateStart,dateEnd )
        //return true
    }

    addErrorResponse( res, msg ){        
        if( typeof res[INV_FIELDS] === UNDEF ){
            res.invalid_fields = []
        }
        res.invalid_fields.push(msg)
    }
}