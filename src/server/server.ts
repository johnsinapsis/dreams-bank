import express, {Router, urlencoded, json} from 'express'
import config from '../config/config.js'
import { RouteManager } from '../route/route.js'

export class ExpressServer{
    app = express();
    router = Router();
    routeManager = new RouteManager(this.router)

    constructor(){
        this.app.use(urlencoded({extended: false}))
        this.app.use(express.json())
        this.app.use(this.router)
    }

    startup(){
        const {port} = config
        this.app.listen(port, function() {
            console.log('Servidor web escuchando en el puerto '+ port)
        });
    }

    getApp(){
        return this.app
    }

    getRouter(){
        return this.router
    }

    getRouteManager(){
        return this.routeManager       
    }

}