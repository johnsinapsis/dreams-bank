import { ProductState } from "./productState";

export class Product{
    id: number
    type: number
    accountNumber: string
    userId: number
    state: ProductState
}