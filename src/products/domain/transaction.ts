export class Transaction{
    id: number
    productId: number
    date: Date
    commerce: string
    tax: number
    mount: number
}