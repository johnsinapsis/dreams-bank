import { BaseValidation } from "../../../validation/base.validation";

export class ValidateFieldsAllTransactionsByProduct extends BaseValidation{
    constructor(){
        super()
        let emptyUser = this.fieldValidationFactory.createInstance('empty','$.userId','El id del usuario es requerido')
        let emptyProduct = this.fieldValidationFactory.createInstance('empty','$.productId','El id del producto es requerido')
        let numberUserId = this.fieldValidationFactory.createInstance('number', '$.userId', 'El id del usuario debe ser numérico');
        let numberProductId = this.fieldValidationFactory.createInstance('number', '$.productId', 'El id del producto debe ser numérico');
        this.addValidator(emptyUser)
        this.addValidator(emptyProduct)
        this.addValidator(numberUserId)
        this.addValidator(numberProductId)
    }
}