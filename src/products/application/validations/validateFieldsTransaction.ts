import { BaseValidation } from "../../../validation/base.validation";

export class ValidateFieldsTransaction extends BaseValidation{
    constructor(){
        super()
        let numberTransactionId = this.fieldValidationFactory.createInstance('number', '$.transactionId', 'El id de la transacción debe ser numérico');
        this.addValidator(numberTransactionId)
    }
}