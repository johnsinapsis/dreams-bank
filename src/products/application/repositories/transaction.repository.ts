import { Transaction } from "../../domain/transaction";

export interface TransactionRepository {
    getTransactionsByProduct(productId:number): Promise <Array <Object>>
    getTransaction(transactionId:number): Promise <Transaction>
    getAverage(req:object): Promise <number>
}