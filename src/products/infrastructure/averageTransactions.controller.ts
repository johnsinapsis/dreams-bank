import { Controller, configControllerIdentity } from "../../controllers/controller.base";
import { BaseErrorHandler } from "../../shared/infrastructure/base.error.handler";
import { ValidateFieldsAvgTransaction } from "../application/validations/validateFieldsAvgTransaction";
import { ErrorAvgTransaction } from "./handler/error.avg.transaction";
import { TransactionPersistence } from "./persistence/transaction.persistence";
import { ExecRulesAverageTransactions } from "./rules/exec.rules.averageTransactions";
import { FormatFields } from "../../shared/infrastructure/format.fields";
import { AverageTransactionsByProduct } from "../application/averageTransactionsByProduct";

export class AverageTransactionsPostController extends Controller implements configControllerIdentity{

    private validateFields
    private rules
    private daoTransactions
    private averageTransactionsByProduct

    constructor(){
        super()
        this.validateFields = new ValidateFieldsAvgTransaction()
        this.rules = new ExecRulesAverageTransactions()
        this.daoTransactions = new TransactionPersistence()
    }

    getConfigId(){
        return 'averageTransactions'
    }

    async postAverageTransactions(req,res){
        try{
            if(this.validateFields.isValid(req,res)){
                let {userId,productId,dateStart, dateEnd} = req.body
                let params = {
                    userId,
                    productId,
                    dateStart:FormatFields.formatDateStart(dateStart),
                    dateEnd: FormatFields.formatDateEnd(dateEnd)
                }
                await this.rules.exec(params)
                let average = await this.daoTransactions.getAverage(params)
                this.averageTransactionsByProduct = new AverageTransactionsByProduct(average)
                return res.json(this.averageTransactionsByProduct.map2Response())
            }
            else{
                let error = new ErrorAvgTransaction()
                return res.json(error.buildResponse(res))
            }
        }catch(e){
            return BaseErrorHandler.handle(res,e)
        }
    }
}