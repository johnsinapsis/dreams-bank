import { sequelize } from "../../../shared/infrastructure/base.persistence";
import { QueryTypes } from "sequelize"
import { Transaction } from "../../domain/transaction";
import { TransactionRepository } from "../../application/repositories/transaction.repository";

export class TransactionPersistence implements TransactionRepository{

    public async getTransactionsByProduct(productId):Promise <Array <Object>>{
        let sql = "SELECT id, commerce,mount,`date` AS fecha FROM transactions  WHERE product_id = ?"
        const transactions: any = await sequelize.query(sql, 
            { 
                replacements: [productId],
                type: QueryTypes.SELECT 
            });
        return transactions
    }

    public async getTransaction(transactionId):Promise <Transaction>{
        const transactions: any = await sequelize.query("SELECT * FROM transactions where id = ?", 
            { 
                replacements: [transactionId],
                type: QueryTypes.SELECT 
            });
        return this.map2response(transactions)
    }

    public async getAverage(req): Promise <number>{
        let sql = "select AVG(mount) as average FROM transactions  WHERE product_id = ? AND `date` BETWEEN ? AND ?"
        const transactions: any = await sequelize.query(sql, 
            { 
                replacements: [req.productId,req.dateStart,req.dateEnd],
                type: QueryTypes.SELECT 
            });
        return transactions[0].average
    }

    private map2response(transactions:any): Transaction{
        if(transactions.length===0)
            return new Transaction
        let transaction:Transaction = {
            id: transactions[0].id,
            productId: transactions[0].product_id,
            date: transactions[0].date,
            commerce: transactions[0].commerce,
            tax: transactions[0].tax,
            mount: transactions[0].mount
        }
        return transaction
    }
}
