import { Sequelize, DataTypes, Model } from 'sequelize'
import { values } from 'sequelize/types/lib/operators'
import { sequelize } from '../../../../shared/infrastructure/base.persistence'

export class ProductModel{
    public product

    constructor(){
        this.product = sequelize.define('product',{
            id: {
                type: DataTypes.INTEGER,
                autoIncrement:true,
                allowNull: false,
                primaryKey: true
              },
            type:{
                type: DataTypes.NUMBER,
            },
            accountNumber: {
                type: DataTypes.STRING,
                field: 'account_number',
            },
            userId: {
                type: DataTypes.NUMBER,
                field: 'user_id',
            },
            state:{
                type: DataTypes.ENUM,
                values: ['Approved','Pending']
            }
        },
        {
            timestamps: false
        }
        )
    }
}
