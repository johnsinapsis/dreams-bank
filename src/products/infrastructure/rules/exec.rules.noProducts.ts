import { ProductPersistence } from "../persistence/product.persistence";
import { BusinessError } from "../../../shared/application/handler/business.error";
import { BusinessNoProduct } from "../../../shared/application/business.rules";

export class ExecRulesNoProduct implements BusinessNoProduct{
    private type = "ExecRulesNoProduct"
    private daoProduct

    constructor(){
        this.daoProduct = new ProductPersistence()
    }

    public async noExistProduct(accountNumber:string): Promise <boolean>{
        let product = await this.daoProduct.getProductByAccount(accountNumber)
        if(product.id){
            return false
        }
        return true
    }
    

    public async exec(accountNumber:string){
        if(!await this.noExistProduct(accountNumber))
            this.throwBusiness("El producto ya existe")
    }

    public throwBusiness(msg){
        let response = {error: msg}
        throw new BusinessError(this.type,response)
    }
}