import { BusinessError } from "../../../shared/application/handler/business.error";
import { ExecRulesUser } from "../../../users/infrastructure/rules/exec.rules.user";

export class ExecRulesGetAllProductsByUser {

    private type = "ExecRulesGetAllProductsByUser"
    private rulesUser

    constructor(){
        this.rulesUser = new ExecRulesUser()
    }

    public async exec(userId:number){
        await this.rulesUser.exec(userId)
    }

}