import { ProductPersistence } from "../persistence/product.persistence";
import { BusinessError } from "../../../shared/application/handler/business.error";
import { BusinessProduct } from "../../../shared/application/business.rules";

export class ExecRulesProduct implements BusinessProduct{
    private type = "ExecRulesProduct"
    private daoProduct

    constructor(){
        this.daoProduct = new ProductPersistence()
    }

    public async existProduct(productId:number): Promise <boolean>{
        let product = await this.daoProduct.getProduct(productId)
        if(!product.id)
            return false
        return true
    }
    

    public async exec(userId:number){
        if(! await this.existProduct(userId))
            this.throwBusiness("El producto no existe")
    }

    public throwBusiness(msg){
        let response = {error: msg}
        throw new BusinessError(this.type,response)
    }
}