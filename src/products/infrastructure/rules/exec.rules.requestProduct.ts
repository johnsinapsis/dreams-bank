import { BusinessError } from "../../../shared/application/handler/business.error";
import { ExecRulesUser } from "../../../users/infrastructure/rules/exec.rules.user";
import { ExecRulesProductType } from "./exec.rules.ProductType";
import { ExecRulesNoProduct } from "./exec.rules.noProducts";

export class ExecRulesRequestProduct{
    
    private type = "ExecRulesRequestProduct"
    private rulesUser
    private rulesProductType
    private rulesNoProduct

    constructor(){
        this.rulesUser = new ExecRulesUser()
        this.rulesProductType = new ExecRulesProductType()
        this.rulesNoProduct = new ExecRulesNoProduct()
    }

    public async exec(req:any){
        await this.rulesUser.exec(req.userId)
        await this.rulesProductType.exec(req.type)
        await this.rulesNoProduct.exec(req.accountNumber)
        
    }

}