-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.13-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Volcando estructura para tabla dreams_bank.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(10) unsigned NOT NULL,
  `account_number` varchar(50) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `state` enum('Approved','Pending') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_number` (`account_number`),
  KEY `FK_products_product_types` (`type`),
  KEY `FK_products_users` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla dreams_bank.products: 3 rows
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `type`, `account_number`, `user_id`, `state`) VALUES
	(1, 1, 'TC456465465', 1, 'Approved'),
	(2, 2, 'C65465465', 1, 'Approved'),
	(3, 3, 'CA7897987', 1, 'Approved'),
	(4, 1, 'YC798798', 1, 'Pending'),
	(5, 1, 'YC798799', 1, 'Pending'),
	(6, 2, 'YC798800', 1, 'Pending'),
	(7, 2, 'YC798801', 1, 'Pending'),
	(8, 2, 'YC798802', 1, 'Pending'),
	(9, 2, 'YC798803', 2, 'Pending'),
	(10, 2, 'YC798804', 2, 'Pending'),
	(11, 2, 'YC798806', 2, 'Pending'),
	(12, 2, 'YC798807', 2, 'Pending'),
	(13, 3, 'AB92083805', 2, 'Pending');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Volcando estructura para tabla dreams_bank.product_types
CREATE TABLE IF NOT EXISTS `product_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla dreams_bank.product_types: 4 rows
/*!40000 ALTER TABLE `product_types` DISABLE KEYS */;
INSERT INTO `product_types` (`id`, `name`) VALUES
	(1, 'Crédito ágil'),
	(2, 'Tarjeta de Crédito'),
	(3, 'Cuenta de ahorros'),
	(4, 'Leasing de vivienda');
/*!40000 ALTER TABLE `product_types` ENABLE KEYS */;

-- Volcando estructura para tabla dreams_bank.sessions
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(200) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sessions_users` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla dreams_bank.sessions: 6 rows
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` (`id`, `user_id`, `date_start`, `date_end`) VALUES
	('b3458fb8-4aa3-4572-a5af-7882ba995bf0', 2, '2021-08-25 15:50:20', NULL),
	('76c82d13-6e8c-4719-a961-d65b4f26ada0', 1, '2021-08-25 08:27:07', NULL),
	('43e01987-0232-41ae-a2e1-50b2ab37ddf3', 1, '2021-08-25 08:26:12', NULL),
	('4e15d98a-3b12-47ea-b4dd-2f7d2d7d2e53', 1, '2021-08-25 08:04:35', NULL),
	('c9d2e985-fe93-469b-9d5e-c2ccebd94120', 1, '2021-08-25 16:52:19', NULL),
	('59ca95b4-9b6c-4b76-a7e0-4be88c9c1296', 1, '2021-08-25 16:36:57', NULL),
	('1837cf29-f15a-4263-8e58-7807590282ba', 1, '2021-08-25 16:24:34', NULL),
	('e2201179-a306-4826-9847-15ee6c4a646b', 1, '2021-08-25 16:21:08', NULL),
	('5750a2f1-35f9-4bf8-9870-bd6ba96ba7ed', 1, '2021-08-25 16:15:54', NULL),
	('7f206704-a1d8-4870-9df2-d00bfd5c84fa', 1, '2021-08-25 16:15:38', NULL),
	('4e105b6f-a436-4d2e-ae13-06ac74855566', 1, '2021-08-25 16:13:57', NULL),
	('c0ae558b-fd49-42cd-a315-24ffd2b41955', 1, '2021-08-25 16:05:20', NULL),
	('3ae4ab44-3466-4fde-9b39-0882191ff155', 1, '2021-08-25 16:04:41', NULL),
	('9ca3ac25-88c9-4fc1-b169-a485217bed4c', 1, '2021-08-25 16:03:35', NULL);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

-- Volcando estructura para tabla dreams_bank.transactions
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `commerce` varchar(200) NOT NULL,
  `tax` double NOT NULL,
  `mount` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_transactions_products` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla dreams_bank.transactions: 5 rows
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` (`id`, `product_id`, `date`, `commerce`, `tax`, `mount`) VALUES
	(1, 1, '2021-08-24 00:17:20', 'pet shop boys', 4.5, 204.5),
	(2, 2, '2021-08-24 00:18:07', 'kassandra', 0.5, 35.5),
	(3, 1, '2021-08-25 00:18:39', 'star bucks', 0, 1.5),
	(4, 1, '2021-08-27 00:19:20', 'kryas', 0.7, 18.8),
	(5, 1, '2021-08-29 00:20:09', 'Netflix', 0, 10);
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;

-- Volcando estructura para tabla dreams_bank.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('Client','No Client') NOT NULL,
  `document_number` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla dreams_bank.users: 2 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `type`, `document_number`, `name`, `surname`, `password`) VALUES
	(1, 'Client', '123456', 'George', 'Diaz', '123456'),
	(2, 'Client', '987654', 'Hanna', 'Roxwell', '123456'),
	(3, 'Client', '192837', 'Rocky', 'Mendez', '123456');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
